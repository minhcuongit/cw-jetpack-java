/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.weather;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.commonsware.jetpack.weather.databinding.ActivityMainBinding;
import com.commonsware.jetpack.weather.databinding.RowBinding;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    final ActivityMainBinding binding =
      ActivityMainBinding.inflate(getLayoutInflater());

    setContentView(binding.getRoot());

    final MainMotor motor = new ViewModelProvider(this).get(MainMotor.class);
    final WeatherAdapter adapter = new WeatherAdapter();

    binding.forecasts.setLayoutManager(new LinearLayoutManager(this));
    binding.forecasts.addItemDecoration(new DividerItemDecoration(this,
      DividerItemDecoration.VERTICAL));
    binding.forecasts.setAdapter(adapter);

    motor.results.observe(this, state -> {
      if (!state.isLoading && state.error == null) {
        binding.progress.setVisibility(View.GONE);
        adapter.submitList(state.forecasts);
      }
      else if (state.error != null) {
        binding.progress.setVisibility(View.GONE);
        Toast.makeText(MainActivity.this, state.error.getLocalizedMessage(),
          Toast.LENGTH_LONG).show();
        Log.e("Weather", "Exception loading data", state.error);
      }
    });

    motor.load("OKX", 32, 34);
  }

  class WeatherAdapter extends ListAdapter<RowState, RowHolder> {
    WeatherAdapter() {
      super(new DiffUtil.ItemCallback<RowState>() {
        @Override
        public boolean areItemsTheSame(@NonNull RowState oldItem,
                                       @NonNull RowState newItem) {
          return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull RowState oldItem,
                                          @NonNull RowState newItem) {
          return oldItem.icon.equals(newItem.icon) &&
            oldItem.name.equals(newItem.name) &&
            oldItem.temp.equals(newItem.temp);
        }
      });
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                        int viewType) {
      return new RowHolder(
        RowBinding.inflate(getLayoutInflater(), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
      holder.bind(getItem(position));
    }
  }

  static class RowHolder extends RecyclerView.ViewHolder {
    final private RowBinding binding;

    RowHolder(RowBinding binding) {
      super(binding.getRoot());

      this.binding = binding;
    }

    void bind(RowState state) {
      binding.setState(state);
      binding.executePendingBindings();
    }
  }
}
