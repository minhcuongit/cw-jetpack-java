/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.dialog;

import androidx.lifecycle.Observer;

public final class Event<T> {
  public interface Handler<T> {
    void handle(T content);
  }

  public static class EventObserver<T> implements Observer<Event<T>> {
    private final Event.Handler<T> handler;

    public EventObserver(Handler<T> handler) {
      this.handler = handler;
    }

    @Override
    public void onChanged(Event<T> event) {
      if (event != null) {
        event.handle(handler);
      }
    }
  }

  private boolean hasBeenHandled = false;
  private final T content;

  public Event(T content) {
    this.content = content;
  }

  private void handle(Event.Handler<T> handler) {
    if (!hasBeenHandled) {
      hasBeenHandled = true;
      handler.handle(content);
    }
  }
}
