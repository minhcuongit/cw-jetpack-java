/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware;

import android.content.Context;
import android.content.res.AssetManager;
import com.google.common.util.concurrent.ListenableFuture;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;
import androidx.concurrent.futures.CallbackToFutureAdapter;

class PassphraseRepository {
  private static final String ASSET_FILENAME = "eff_short_wordlist_2_0.txt";
  private static volatile PassphraseRepository INSTANCE;

  synchronized static PassphraseRepository get(Context context) {
    if (INSTANCE == null) {
      INSTANCE = new PassphraseRepository(context.getApplicationContext());
    }

    return INSTANCE;
  }

  private final AssetManager assets;
  private final AtomicReference<List<String>> wordsCache =
    new AtomicReference<>();
  private final SecureRandom random = new SecureRandom();
  private final Executor threadPool = Executors.newSingleThreadExecutor();

  private PassphraseRepository(Context context) {
    assets = context.getAssets();
  }

  ListenableFuture<List<String>> generate(int wordCount) {
    return CallbackToFutureAdapter.getFuture(completer -> {
      threadPool.execute(() -> {
        try {
          List<String> words = wordsCache.get();

          if (words == null) {
            InputStream in = assets.open(PassphraseRepository.ASSET_FILENAME);

            words = readWords(in);
            in.close();

            synchronized (wordsCache) {
              wordsCache.set(words);
            }
          }

          completer.set(rollDemBones(words, wordCount));
        }
        catch (Throwable t) {
          completer.setException(t);
        }
      });

      return "generate words";
    });
  }

  private List<String> rollDemBones(List<String> words, int wordCount) {
    List<String> result = new ArrayList<>();
    int size = words.size();

    for (int i = 0; i < wordCount; i++) {
      result.add(words.get(random.nextInt(size)));
    }

    return result;
  }

  private List<String> readWords(InputStream in) throws IOException {
    InputStreamReader isr = new InputStreamReader(in);
    BufferedReader reader = new BufferedReader(isr);
    String line;
    List<String> result = new ArrayList<>();

    while ((line = reader.readLine()) != null) {
      String[] pieces = line.split("\\s");

      if (pieces.length == 2) {
        result.add(pieces[1]);
      }
    }

    return result;
  }
}
