/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware;

import android.app.Application;
import android.net.Uri;
import android.text.TextUtils;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class MainMotor extends AndroidViewModel {
  private static final int DEFAULT_WORD_COUNT = 6;
  private final PassphraseRepository repo;
  private Uri wordsDoc = PassphraseRepository.ASSET_URI;
  final MutableLiveData<MainViewState> viewStates =
    new MutableLiveData<>();

  public MainMotor(@NonNull Application application) {
    super(application);

    repo = PassphraseRepository.get(application);
    generatePassphrase(DEFAULT_WORD_COUNT);
  }

  void generatePassphrase() {
    final MainViewState current = viewStates.getValue();

    if (current == null) {
      generatePassphrase(DEFAULT_WORD_COUNT);
    }
    else {
      generatePassphrase(current.wordCount);
    }
  }

  void generatePassphrase(int wordCount) {
    viewStates.setValue(new MainViewState(true, null, wordCount, null));

    ListenableFuture<List<String>> future = repo.generate(wordsDoc, wordCount);

    future.addListener((Runnable)() -> {
      try {
        viewStates.postValue(new MainViewState(false,
          TextUtils.join(" ", future.get()), wordCount, null));
      }
      catch (Exception e) {
        viewStates.postValue(new MainViewState(false, null, wordCount, e));
      }
    }, Runnable::run);
  }

  void generatePassphrase(Uri wordsDoc) {
    this.wordsDoc = wordsDoc;

    generatePassphrase();
  }
}
